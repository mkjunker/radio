#!/usr/bin/env python3
'''Currently uses the media library.

If the library has only one entry, doesn't work well.

TO DO:
Allow using different files.

Set up libraries so the streaming stuff is in one area.'''
import argparse
import logging
import sys
import xmltodict
import subprocess
import platform
import os

minver = 3,6
args = None

def parse_args(argv, library):
    '''Parse arguments, set up logging.'''
    global args

    parser = argparse.ArgumentParser(
        description='')

    verbose_help = '-hh' in argv

    # See argparse.FileType

    parser.add_argument('-hh', action='help',
                        help="show verbose help and exit")
    parser.add_argument('-hm',
                        action='store_true',
                        required=False,
                        help=('Module help' if verbose_help
                              else argparse.SUPPRESS))
    for l in library:
        try:
            parser.add_argument('--' + l['title'].replace(' ', '-').lower(),
                                action='store_const',
                                const=l['location'],
                                dest='url')
        except:
            for ll in l:
                print(l[ll])
    parser.add_argument('--list', '-l', action='store_true')
    parser.add_argument('--off', '--kill', action='store_true')
    parser.add_argument('--gui', '-g', action='store_true')
    loglevels = list(logging._levelToName.values())
    loglevels.remove('NOTSET')
    loglevels = list(map(str.lower, loglevels))
    parser.add_argument('--log',
                        required=False,
                        metavar='LEVEL',
                        choices=loglevels,
                        help=('set logging level: ' + str(loglevels)
                              + ' [%(default)s]' if verbose_help
                              else argparse.SUPPRESS),
                        default='warning')
    parser.add_argument('--log-file', '-lf',
                        required=False,
                        help=('Log file name, '
                              'in addition to stderr'
                              if verbose_help
                              else argparse.SUPPRESS),
                        metavar='PATH')

    args = parser.parse_args(args=argv)
    level = getattr(logging, args.log.upper())
    handlers = [logging.StreamHandler(sys.stderr)]
    if args.log_file is not None:
        handlers.append(logging.FileHandler(args.log_file))
    logging.basicConfig(level=level,
                        handlers=handlers)
    if args.hm:
        help(__name__)
        sys.exit(0)

    return args.url

def checkminver():
    '''Insure the needed python version is used.

If an insufficient version is used, exit with code 1.'''
    if sys.version_info < minver:
        logging.critical(
            'Minimum python version is '+'.'.join(map(str,minver)))
        logging.critical(
            'Using '+'.'.join(map(str,sys.version_info)))
        sys.exit(1)

def listem(libfile, lib):
    print(libfile)
    for l in lib:
        print('{} {}'.format(l['title'], l['location']))


def main(argv=None):
    if argv is None:
        argv = sys.argv[1:]

    dir = os.path.dirname(os.path.realpath(sys.argv[0]))
    libfile = os.path.join(dir, 'streams.xspf')


    windows = (platform.system() == 'Windows'
               or platform.system().startswith('CYGWIN'))
    if windows:
        libfile = os.path.join(os.environ['APPDATA'], r'vlc\ml.xspf')
    else:
        print('Platform not supported.')
        sys.exit(2)

    # FIXME: we're reading argument names from a file, so we can't
    # specify the file on the command line
    with open(libfile, 'r') as f:
        t = f.read()
        library = xmltodict.parse(t)['playlist']['trackList']['track']

    url = parse_args(argv, library)
    logging.debug(libfile)
    logging.debug(library)
    checkminver()
    if args.list:
        listem(libfile, library)
        return 0

    if windows:
        if args.off:
            subprocess.run(['taskkill', '/f', '/im', 'vlc.exe'])
        else:
            if not url:
                print('At least one argument is required.')
                sys.exit(1)
            if args.gui:
                subprocess.run(['cmd', '/c', 'start', 'vlc', url, '&'])
            else:
                subprocess.run(['cmd', '/c', 'start', 'vlc',
                                '--intf', 'dummy',
                                url, '&'])
#    subprocess.run(['start cmd /k vlc', url, '&'])
    return 0

if __name__ == "__main__":
    sys.exit(main())
